import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimerComponent } from './components/timer/timer.component';
import { TimersComponent } from './components/timers/timers.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SummaryComponent } from './components/summary/summary.component';
import { ExportComponent } from './components/export/export.component';

const routes: Routes = [
	{ path: '', component: TimersComponent },
	{ path: 'settings', component: SettingsComponent },
	{ path: 'timer/:timerId', component: TimerComponent },
	{ path: 'summary', component: SummaryComponent },
	{ path: 'export', component: ExportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
