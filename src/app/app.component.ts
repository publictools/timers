import { Component } from '@angular/core';
import { PersistenceService } from './services/persistence.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private persistenceService: PersistenceService) {
    this.persistenceService.loadFromLocalStorage();
  }

}
