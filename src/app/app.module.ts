import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimersComponent } from './components/timers/timers.component';
import { TimersService } from './services/timers.service';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsService } from './services/settings.service';
import { PersistenceService } from './services/persistence.service';
import { TimerComponent } from './components/timer/timer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { PeriodComponent } from './components/period/period.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PeriodUpdateResultModalComponent } from './components/period-update-result-modal/period-update-result-modal.component';
import { SummaryComponent } from './components/summary/summary.component';
import { SelectedDateService } from './services/selected-date.service';
import { DateSelectorComponent } from './components/date-selector/date-selector.component';
import { AddNoteDialogComponent } from './components/add-note-dialog/add-note-dialog.component';
import { ExportComponent } from './components/export/export.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [
    AppComponent,
    TimersComponent,
    SettingsComponent,
    TimerComponent,
    PeriodComponent,
    PeriodUpdateResultModalComponent,
    SummaryComponent,
    DateSelectorComponent,
    AddNoteDialogComponent,
    ExportComponent
  ],
  entryComponents: [
    PeriodUpdateResultModalComponent,
    AddNoteDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatExpansionModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    MatDividerModule
  ],
  providers: [
    TimersService,
    SettingsService,
    PersistenceService,
    SelectedDateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
