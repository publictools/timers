import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-note-dialog',
  templateUrl: './add-note-dialog.component.html',
  styleUrls: ['./add-note-dialog.component.css']
})
export class AddNoteDialogComponent implements OnInit {

  newTimerNote = new FormControl('');

  constructor(public dialogRef: MatDialogRef<AddNoteDialogComponent>) { }

  ngOnInit() {
  }

  addNote() {
    this.dialogRef.close(this.newTimerNote.value);
  }

}
