import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SelectedDateService } from './../../services/selected-date.service';
import { TimersService } from './../../services/timers.service';
import { Utils } from './../../models/utils';

@Component({
  selector: 'app-date-selector',
  templateUrl: './date-selector.component.html',
  styleUrls: ['./date-selector.component.css']
})
export class DateSelectorComponent implements OnInit {

  dateSelector = new FormControl('');

  constructor(public timersService: TimersService,
              public selectedDateService: SelectedDateService
             ) {}

  ngOnInit() {
    this.dateSelector.setValue(this.selectedDateService.selectedDate.toISOString().substring(0,10));
    this.dateSelector.valueChanges.subscribe(v => {
    	this.selectedDateService.selectedDate = new Date(v);
    });
  }

  formatTime(miliSeconds: number): string {
    return Utils.formatTime(miliSeconds);
  }

}
