import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TimersService } from './../../services/timers.service';
import { SettingsService } from './../../services/settings.service';
import { ExportDataForTimer } from './../../models/export-data-for-timer';
import { ExportDataLine } from './../../models/export-data-line';
import { utils, writeFileXLSX } from 'xlsx';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  dateSelectorStart = new FormControl('');
  dateSelectorEnd = new FormControl('');
  exportDataForDateString: string;

  constructor( private timersService: TimersService,
               private settingsService: SettingsService ) { }

  ngOnInit() {
    this.dateSelectorStart.setValue((new Date()).toISOString().substring(0,10));
    this.dateSelectorEnd.setValue((new Date()).toISOString().substring(0,10));
  }

  generateExport() {
    const startDate = new Date(this.dateSelectorStart.value);
    const endDate = new Date(this.dateSelectorEnd.value);
    const days: Date[] = [];
    if (startDate <= endDate) {
      let d = new Date(startDate);
      while (d <= endDate) {
        days.push(new Date(d));
        d.setDate(d.getDate() + 1);
      }
    }
    this.exportDataForDateString = "Date;Hours;Project;Subproject;Task;Comment;Creative Status;Location;\n";
    const exportDataList: ExportDataLine[] = [];
    this.timersService.timerList.map(t => new ExportDataForTimer(t, days)).forEach(edft => {
        edft.exportDataForDateList
          .filter(edfd => !edfd.isEmpty())
          .map(edfd => new ExportDataLine(edfd.date.toISOString().slice(0,10), this.getHoursRounded(edfd.time), edfd.timer.project, edfd.timer.subproject, edfd.timer.task, edfd.notes, "n/a", this.timersService.location))
          .forEach(edl => exportDataList.push(edl));
    });
    this.exportDataForDateString = this.exportDataForDateString + exportDataList.map(edl => edl.toString() + "\n").join("");
    this.exportDataForDateString = this.exportDataForDateString.replace('. . ', '. ');
    const ws = utils.json_to_sheet(exportDataList);
    utils.sheet_add_aoa(ws, [["Date", "Hours", "Project", "Subproject", "Task", "Comment", "Creative Status", "Location"]], { origin: "A1" });
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Entries");
    writeFileXLSX(wb, "TimeExport.xlsx");
  }

  getHoursRounded(hours) {
    const integerPart = Math.floor(hours);
    const fractionalPart = hours - integerPart;
    console.log("fractionalPart: " + fractionalPart);
    console.log("this.settingsService.roundingStrategy.l1: " + this.settingsService.roundingStrategy.l1);
    let finalFract = 0;
    if (fractionalPart > this.settingsService.roundingStrategy.l4) {
      finalFract = 1;
    } else if (fractionalPart > this.settingsService.roundingStrategy.l3) {
      finalFract = 0.75;
    } else if (fractionalPart > this.settingsService.roundingStrategy.l2) {
        finalFract = 0.5;
    } else if (fractionalPart > this.settingsService.roundingStrategy.l1) {
        finalFract = 0.25;
    }
    return integerPart + finalFract;
  }

}
