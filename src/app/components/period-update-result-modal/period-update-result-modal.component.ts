import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-period-update-result-modal',
  templateUrl: './period-update-result-modal.component.html'
})
export class PeriodUpdateResultModalComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

}

