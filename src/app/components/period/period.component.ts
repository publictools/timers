import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TimePeriod } from '../../models/time-period';
import { TimersService } from '../../services/timers.service';
import { MatDialog } from '@angular/material/dialog';
import { PeriodUpdateResultModalComponent } from './../period-update-result-modal/period-update-result-modal.component';

@Component({
  selector: 'app-period',
  templateUrl: './period.component.html',
  styleUrls: ['./period.component.css']
})
export class PeriodComponent implements OnInit {

  @Input() timePeriod: TimePeriod;
  @Output() timePeriodChange = new EventEmitter<string>();

  periodUpdateForm = new FormGroup({
    sideSelector : new FormControl('', Validators.required),
    directionSelector : new FormControl('', Validators.required),
    minutesAmount : new FormControl('', [Validators.required, Validators.pattern('[0-9]+')])
  });

  constructor(private timersService: TimersService, public dialog: MatDialog) { }

  ngOnInit() {
  }

  update() {

    let amount = this.periodUpdateForm.value.minutesAmount * 1000 * 60;
    if (this.periodUpdateForm.value.directionSelector == "BACKWARD") {
    	amount = -1 * amount;
    }
    let newStart = this.timePeriod.start;
    if (this.periodUpdateForm.value.sideSelector == "START") {
      newStart = newStart + amount;
    }
    let newEnd = this.timePeriod.end;
    if (this.periodUpdateForm.value.sideSelector == "END") {
      newEnd = newEnd + amount;
    }

    const modifiedTimePeriod = TimePeriod.resolveFromAny({
    	"start" : newStart,
    	"end" : newEnd
    });

    const result = this.timersService.update(this.timePeriod, modifiedTimePeriod);

    this.dialog.open(PeriodUpdateResultModalComponent, { data : {
      status : result.success,
      message : result.message
    }}).afterClosed().subscribe(result => {
      this.timePeriodChange.emit('modalClosed');
    });

  }

}
