import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SettingsService } from './../../services/settings.service';
import { TimersService } from './../../services/timers.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  refreshInterval = new FormControl(1);
  timeWarningLimit = new FormControl(1);
  location = new FormControl("");
  roundingStrategyL1 = new FormControl(0.1);
  roundingStrategyL2 = new FormControl(0.3);
  roundingStrategyL3 = new FormControl(0.6);
  roundingStrategyL4 = new FormControl(0.8);

  constructor(private settingsService: SettingsService, private timersService: TimersService) { }

  ngOnInit() {
    this.refreshInterval.setValue(this.settingsService.refreshInterval);
    this.refreshInterval.valueChanges.subscribe(v => {
    	this.settingsService.refreshInterval = v;
    });
    this.timeWarningLimit.setValue(this.settingsService.timeWarningLimit);
    this.timeWarningLimit.valueChanges.subscribe(v => {
    	this.settingsService.timeWarningLimit = v;
    });
    this.location.setValue(this.timersService.location);
    this.location.valueChanges.subscribe(v => {
    	this.timersService.location = v;
    });

    this.roundingStrategyL1.setValue(this.settingsService.roundingStrategy.l1);
    this.roundingStrategyL1.valueChanges.subscribe(v => {
    	this.settingsService.roundingStrategy.l1 = parseFloat(v);
    });
    this.roundingStrategyL2.setValue(this.settingsService.roundingStrategy.l2);
    this.roundingStrategyL2.valueChanges.subscribe(v => {
    	this.settingsService.roundingStrategy.l2 = parseFloat(v);
    });
    this.roundingStrategyL3.setValue(this.settingsService.roundingStrategy.l3);
    this.roundingStrategyL3.valueChanges.subscribe(v => {
    	this.settingsService.roundingStrategy.l3 = parseFloat(v);
    });
    this.roundingStrategyL4.setValue(this.settingsService.roundingStrategy.l4);
    this.roundingStrategyL4.valueChanges.subscribe(v => {
    	this.settingsService.roundingStrategy.l4 = parseFloat(v);
    });
  }

}
