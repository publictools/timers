import { Component, OnInit } from '@angular/core';
import { TimersService } from './../../services/timers.service';
import { SelectedDateService } from './../../services/selected-date.service';
import { Utils } from './../../models/utils';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  constructor(private timersService: TimersService,
              public selectedDateService: SelectedDateService) { }

  ngOnInit() {
  }

  formatTime(miliSeconds: number): string {
    return Utils.formatTime(miliSeconds);
  }

  getNonEmptyTimers() {
	  return this.timersService.timerList.filter(t => t.getFinishedTimeCountersSum(this.selectedDateService.selectedDate) > 0);
  }

}
