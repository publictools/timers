import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Timer } from './../../models/timer';
import { TimePeriod } from './../../models/time-period';
import { Utils } from './../../models/utils';
import { DayStatistics } from './../../models/day-statistics';
import { TimersService } from './../../services/timers.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

  timer: Timer | undefined;
  timerName = new FormControl("");
  timerProject = new FormControl("");
  timerSubproject = new FormControl("");
  timerTask = new FormControl("");
  timerArchived = new FormControl(false);
  timerDays: DayStatistics[] = [];
  allDays = 0;
  showLastDays = 7;
  showLastDaysField = new FormControl(7);

  constructor(private route: ActivatedRoute, private timersService: TimersService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.timer = this.timersService.getTimer(params.get('timerId'));
      this.timerName.setValue(this.timer.name);
      this.timerName.valueChanges.subscribe(v => {
      	this.timer.name = v;
      });
      this.timerProject.setValue(this.timer.project);
      this.timerProject.valueChanges.subscribe(v => {
        this.timer.project = v;
      });
      this.timerSubproject.setValue(this.timer.subproject);
      this.timerSubproject.valueChanges.subscribe(v => {
        this.timer.subproject = v;
      });
      this.timerTask.setValue(this.timer.task);
      this.timerTask.valueChanges.subscribe(v => {
        this.timer.task = v;
      });
      this.timerArchived.setValue(this.timer.archived);
      this.timerArchived.valueChanges.subscribe(v => {
        this.timer.archived = v;
      });
      this.buildViewObjects(this.timer);
    });
  }

  updateView() {
    this.buildViewObjects(this.timer);
  }

  buildViewObjects(timer: Timer) {
    this.timerDays = [];
    timer.finishedPeriodList.forEach(p => {
      const dayStr = new Date(p.start).toISOString().substring(0, 10);
      let existingDay = this.timerDays.find(d => d.day == dayStr);
      if (!existingDay) {
        existingDay = new DayStatistics(dayStr);
        this.timerDays.unshift(existingDay);
      }
      existingDay.add(p);
    });
    this.allDays = this.timerDays.length;
    if (this.timerDays.length > this.showLastDays) {
    	this.timerDays = this.timerDays.slice(0, this.showLastDays);
    }
  }

  getPeriodStart(time: number): string {
	const date = new Date(time);
    return date.toISOString().substring(11, 19);
  }

  getPeriodEnd(time: number): string {
	const date = new Date(time);
    return date.toISOString().substring(11, 19);
  }

  getPeriodTime(period: TimePeriod): string {
    return Utils.formatTime(period.getTime());
  }

  remove(period: TimePeriod) {
    if (confirm("Are you sure? This cannot be undone!")) {
      const index = this.timer.finishedPeriodList.indexOf(period);
      this.timer.finishedPeriodList.splice(index, 1);
      this.buildViewObjects(this.timer);
    }
  }

  updateShowLastDays() {
    if (this.showLastDaysField.value < 10 || confirm("Are you sure? This may slow down this page!")) {
      this.showLastDays = this.showLastDaysField.value;
      this.updateView();
    }
  }

}
