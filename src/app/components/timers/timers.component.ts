import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TimersService } from './../../services/timers.service';
import { SettingsService } from './../../services/settings.service';
import { PersistenceService } from './../../services/persistence.service';
import { SelectedDateService } from './../../services/selected-date.service';
import { Timer } from './../../models/timer';
import { Utils } from './../../models/utils';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { AddNoteDialogComponent } from './../add-note-dialog/add-note-dialog.component';

@Component({
  selector: 'app-timers',
  templateUrl: './timers.component.html',
  styleUrls: ['./timers.component.css']
})
export class TimersComponent implements OnInit, OnDestroy {

  newTimerName = new FormControl('');
  interval: any;
  numberOfColumns: number;
  rowHeight: number;

  constructor(private timersService: TimersService,
              private settingsService: SettingsService,
              private persistenceService: PersistenceService,
              public selectedDateService: SelectedDateService,
              private ref: ChangeDetectorRef,
              private breakpointObserver: BreakpointObserver,
              private dialog: MatDialog) {}

  ngOnInit() {
	this.numberOfColumns = this.breakpointObserver.isMatched('(max-width: 599px)') ? 4 : 8;
	this.rowHeight = this.numberOfColumns == 8 ? 3 : 5;
    this.interval = setInterval(() => {
      this.ref.detectChanges();
      this.persistenceService.saveState();
    }, this.settingsService.refreshInterval * 1000);
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  isStartButtonDisabled(): boolean {
    const sDate = new Date();
    return sDate.getFullYear() != this.selectedDateService.selectedDate.getFullYear() || sDate.getMonth() != this.selectedDateService.selectedDate.getMonth() || sDate.getDate() != this.selectedDateService.selectedDate.getDate();
  }

  isSummaryButtonDisabled(): boolean {
    return this.timersService.isDateSelectorDisabled();
  }

  addTimer() {
    let t = new Timer(this.newTimerName.value, crypto['randomUUID']());
    this.timersService.timerList.push(t);
    this.newTimerName.setValue('');
  }

  getTimers(archive: boolean): Timer[] {
    return this.timersService.timerList.filter(t => t.archived == archive || (archive == false && t.archived == undefined));
  }

  start(timer: Timer) {
    this.timersService.start(timer);
  }

  stop(timer: Timer) {
    this.timersService.stop(timer);
  }

  remove(timer: Timer) {
    this.timersService.remove(timer);
  }

  formatTime(miliSeconds: number): string {
    return Utils.formatTime(miliSeconds);
  }

  getHeaderStyleColor(): string {
    const timeWarningLimitInMilis = this.settingsService.timeWarningLimit * 60 * 60 * 1000;
    return this.selectedDateService.getSumOfTimeInSelectedDate() > timeWarningLimitInMilis ? 'warn' : 'primary';
  }

  openAddNoteDialog(timer: Timer) {
    let dialogRef = this.dialog.open(AddNoteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
        timer.addNote(result);
      });
  }

}
