import { TimePeriod } from './time-period';
import { Utils } from './utils';

export class DayStatistics {

  periodList: TimePeriod[] = [];

  constructor(public day: string) {}

  add(period: TimePeriod) {
    this.periodList.push(period);
  }

  getSumOfTimeStr() {
    const time = this.periodList.map(p => p.getTime()).reduce((a, b) => a + b, 0);
    return Utils.formatTime(time);
  }

}
