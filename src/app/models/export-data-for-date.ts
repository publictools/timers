import { Timer } from './timer';
import { Utils } from './utils';

export class ExportDataForDate {

  time: number;
  notes: string;

  constructor( public timer: Timer, public date: Date ) {
    const sum = timer.getFinishedTimeCountersSum(date);
    if (sum > 0) {
      this.time = Utils.formatTimeForExport(sum);
      this.notes = timer.getFinishedTimeCountersNotes(date);
    }
  }

  isEmpty() {
    return this.time == null;
  }

}
