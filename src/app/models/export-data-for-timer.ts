import { Timer } from './timer';
import { ExportDataForDate } from './export-data-for-date';

export class ExportDataForTimer {

  exportDataForDateList: ExportDataForDate[];

  constructor( public timer: Timer, public days: Date[] ) {
    this.exportDataForDateList = days.map(d => new ExportDataForDate(timer, d));
  }

}
