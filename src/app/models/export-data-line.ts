
export class ExportDataLine {

  constructor( public Date: string,
               public Hours: number,
               public Project: string,
               public Subproject: string,
               public Task: string,
               public Comment: string,
               public CreativeStatus: string,
               public Location: string) {}

  toString() {
    return this.Date + ";" + this.Hours + ";" + this.Project + ";" + this.Subproject + ";" + this.Task + ";" + this.Comment + ";" + this.CreativeStatus + ";" + this.Location + ";";
  }

}
