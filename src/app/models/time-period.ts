export class TimePeriod {

  start: number;
  end: number;
  noteList: string[];

  constructor() {
    this.start = Date.now();
  }

  getTime(): number {
    return this.end - this.start;
  }

  startedAt(date: Date): boolean {
    const sDate = new Date(this.start);
    return sDate.getFullYear() == date.getFullYear() && sDate.getMonth() == date.getMonth() && sDate.getDate() == date.getDate();
  }

  hasNoCommonPeriodWith(period: TimePeriod): boolean {
    return period.start > this.end || period.end < this.start; 
  }

  static resolveFromAny(obj: any): TimePeriod {
    const result = new TimePeriod();
    result.start = obj.start;
    result.end = obj.end;
    result.noteList = obj.noteList;
    return result;
  }

  getNotes(): string {
    if (this.noteList == null) {
      return "";
    } else {
      return this.noteList
    		  		.filter(note => note != null)
    		  		.filter(note => note != "")
    		  		.reduce((a,b) => a + ". " + b, "");
    }
  }

}
