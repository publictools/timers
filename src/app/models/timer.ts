import { TimePeriod } from './time-period';
import { TimerState } from './timer-state';

export class Timer {

  activeCounter: TimePeriod;
  finishedPeriodList: TimePeriod[] = [];
  state: TimerState = TimerState.STOPPED;
  projectSubprojectTask: string;
  project: string;
  subproject: string;
  task: string;
  archived: boolean = false;

  constructor(public name: string, public uuid: string) {}

  isStopped(): boolean {
    return this.state == TimerState.STOPPED;
  }

  isStarted(): boolean {
    return this.state == TimerState.STARTED;
  }

  start() {
    if (this.isStopped()) {
      this.activeCounter = new TimePeriod();
      this.state = TimerState.STARTED;
    } else {
      throw new Error('This counter ' + this.name + ' is in ' + this.state + ' state and it cannot be started.');
    }
  }

  stop() {
    if (this.isStarted()) {
      this.activeCounter.end = Date.now();
      this.finishedPeriodList.push(this.activeCounter);
      this.activeCounter = null;
      this.state = TimerState.STOPPED;
    } else {
      throw new Error('This counter ' + this.name + ' is in ' + this.state + ' state and it cannot be stopped.');
    }
  }

  getCurrentTimeCounter(): number {
    if (this.isStarted()) {
      return Date.now() - this.activeCounter.start;
    } else {
      return 0;
    }
  }

  getFinishedTimeCounters(date: Date): TimePeriod[] {
	  return this.finishedPeriodList
                 .filter(tp => tp.startedAt(date));
  }

  getFinishedTimeCountersNotes(date: Date): string {
	  return this.getFinishedTimeCounters(date)
                 .map(tp => tp.getNotes())
                 .filter(notes => notes != "")
                 .filter(notes => notes != null)
                 .reduce((a,b) => a + ". " + b, "");
  }

  getFinishedTimeCountersSum(date: Date): number {
    return this.getFinishedTimeCounters(date)
               .map(tp => tp.getTime())
               .reduce((a,b) => a + b, 0);
  }

  static resolveFromAny(obj: any): Timer {
    const uuid = obj.uuid ? obj.uuid : crypto['randomUUID']();
    const result = new Timer(obj.name, uuid);
    result.activeCounter = obj.activeCounter ? TimePeriod.resolveFromAny(obj.activeCounter) : null;
    result.finishedPeriodList = obj.finishedPeriodList.map(e => TimePeriod.resolveFromAny(e));
    result.state = Timer.resolveState(obj.state);
    result.projectSubprojectTask = obj.projectSubprojectTask;
    result.project = obj.project;
    result.subproject = obj.subproject;
    result.task = obj.task;
    result.archived = obj.archived ? obj.archived : false;
    return result;
  }

  static resolveState(obj:any): TimerState {
    if (obj == 'STARTED') {
    	return TimerState.STARTED;
    } else {
    	return TimerState.STOPPED;
    }
  }

  isEmpty() {
    return this.activeCounter == null && this.finishedPeriodList.length == 0;
  }

  addNote(note: string) {
    if (this.activeCounter.noteList == undefined) {
    	this.activeCounter.noteList = [];
    }
    this.activeCounter.noteList.push(note);
  }

}
