export class UpdateResult {

  constructor(public success: boolean, public message: string) {}

}
