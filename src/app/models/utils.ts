
export class Utils {

  static formatTime(miliSeconds: number): string {
    let roundedToSeconds = Math.round(miliSeconds/1000);
    let secondsPart = roundedToSeconds % 60;
    let roundedToMinutes = (roundedToSeconds - secondsPart) / 60;
    let minutesPart = roundedToMinutes % 60;
    let roundedToHours = (roundedToMinutes - minutesPart) / 60;
    let hoursPart = roundedToHours;
    let result = ' ' + secondsPart + 's';
    if (minutesPart > 0) {
      result = ' ' + minutesPart + 'm:' + result;
    }
    if (hoursPart > 0) {
      result = ' ' + hoursPart + 'h:' + minutesPart + "m";
    }
    return result;
  }

  static formatTimeForExport(miliSeconds: number): number {
    let roundedToSeconds = Math.round(miliSeconds/1000);
    let secondsPart = roundedToSeconds % 60;
    let roundedToMinutes = (roundedToSeconds - secondsPart) / 60;
    let minutesPart = roundedToMinutes % 60;
    let roundedToHours = (roundedToMinutes - minutesPart) / 60;
    let hoursPart = roundedToHours;
    return hoursPart + (minutesPart/60);
  }

}
