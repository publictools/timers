import { Injectable } from '@angular/core';
import { TimersService } from './timers.service';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class PersistenceService {

  localStorageKey = 'timersService';
  localStorageLocationKey = 'timersServiceLocation';
  localStorageSettingsRoundingStrategyKey = 'settingsServiceRoundingStrategy';

  constructor(private timersService: TimersService, private settingsService: SettingsService) { }

  saveState() {
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.timersService.timerList));
    localStorage.setItem(this.localStorageLocationKey, this.timersService.location);
    localStorage.setItem(this.localStorageSettingsRoundingStrategyKey, JSON.stringify(this.settingsService.roundingStrategy));
  }

  loadFromLocalStorage() {
    let stateInJson = localStorage.getItem(this.localStorageKey);
    if (stateInJson) {
      this.timersService.recreateState(stateInJson);
    }
    stateInJson = localStorage.getItem(this.localStorageLocationKey);
    if (stateInJson) {
      this.timersService.location = stateInJson;
    }
    stateInJson = localStorage.getItem(this.localStorageSettingsRoundingStrategyKey);
    if (stateInJson) {
      this.settingsService.recreateRoundingStrategyState(stateInJson);
    }
  }

}
