import { TestBed } from '@angular/core/testing';

import { SelectedDateService } from './selected-date.service';

describe('SelectedDateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SelectedDateService = TestBed.get(SelectedDateService);
    expect(service).toBeTruthy();
  });
});
