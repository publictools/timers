import { Injectable } from '@angular/core';
import { TimersService } from './timers.service';

@Injectable({
  providedIn: 'root'
})
export class SelectedDateService {

  selectedDate: Date = new Date();

  constructor(private timersService: TimersService) { }

  getSumOfTimeInSelectedDate(): number {
    return this.timersService
               .timerList
               .map(t => t.getFinishedTimeCountersSum(this.selectedDate) + t.getCurrentTimeCounter())
               .reduce((a,b) => a + b, 0);
  }

}
