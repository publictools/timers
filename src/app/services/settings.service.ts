import { Injectable } from '@angular/core';
import { RoundingStrategy } from './../models/rounding-strategy';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  refreshInterval: number = 1;
  timeWarningLimit: number = 7.5;
  roundingStrategy: RoundingStrategy = new RoundingStrategy(0.1, 0.3, 0.6, 0.8);

  constructor() { }

  recreateRoundingStrategyState(json: string) {
	  const obj = JSON.parse(json);
	  this.roundingStrategy = new RoundingStrategy(obj.l1, obj.l2, obj.l3, obj.l4);
  }

}
