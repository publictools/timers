import { Injectable } from '@angular/core';
import { Timer } from './../models/timer';
import { TimePeriod } from './../models/time-period';
import { UpdateResult } from './../models/update-result';

@Injectable({
  providedIn: 'root'
})
export class TimersService {

  readonly timerList: Timer[] = [];
  location: string;

  constructor() {}

  start(timer: Timer) {
    this.timerList.filter(t => t.isStarted()).forEach(t => t.stop());
    setTimeout(this.pStart, 2, timer, this.timerList);
  }

  pStart(timer: Timer, timerList: Timer[]) {
    timerList.find(e => e === timer).start();
  }

  stop(timer: Timer) {
    this.timerList.find(e => e === timer).stop();
  }

  remove(timer: Timer) {
    const i = this.timerList.findIndex(e => e === timer);
    this.timerList.splice(i, 1);
  }

  recreateState(json: string) {
    const t: Array<Timer> = JSON.parse(json);
    const result = t.map(e => Timer.resolveFromAny(e));
    this.timerList.length = 0;
    this.timerList.push(...result);
  }

  getTimer(uuid: string) : Timer {
    return this.timerList.find(t => t.uuid == uuid);
  }

  update(originalTimePeriod: TimePeriod, modifiedTimePeriod: TimePeriod): UpdateResult {
    if (modifiedTimePeriod.start >= modifiedTimePeriod.end) {
    	return new UpdateResult(false, "Cannot update period with this parameters, start would be after end.");
    }
    if (this.timerList.find(timer => timer.isStarted())) {
    	return new UpdateResult(false, "Cannot update period while any timer is running. Stop the timer and try again.");
    }
    let result = null;
    this.timerList.forEach(timer => {
      if (timer.finishedPeriodList.find(period => period !== originalTimePeriod && !period.hasNoCommonPeriodWith(modifiedTimePeriod))) {
      	result = new UpdateResult(false, "Cannot update period, it would overlap with period from timer: " + timer.name);
      }
    });
    if (result == null) {
      result = new UpdateResult(true, "Period successfuly updated.");
      originalTimePeriod.start = modifiedTimePeriod.start;
      originalTimePeriod.end = modifiedTimePeriod.end;
    }
    return result;
  }

  isDateSelectorDisabled(): boolean {
    return this.timerList.findIndex(t => t.isStarted()) != -1;
  }

}
